package com.qhstudio.feelingnode.fragment;

import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.base.BaseFragment;
import com.qhstudio.feelingnode.views.CustomViewPageTitle;
import com.qhstudio.feelingnode.views.CustomViewPageTitle.SelectListener;
import com.qhstudio.feelingnode.views.CustomViewPager;

import android.support.v4.app.FragmentManager;

public class HomeFragment extends BaseFragment {
	CustomViewPageTitle pageTitle;
	CustomViewPager viewPage;

	ListFragment hotFragment;
	ListFragment allFragment;

	FragmentManager fragmentManager;
	String hotLable, allLable;

	public HomeFragment(FragmentManager fragmentManager) {
		// TODO Auto-generated constructor stub
		this.fragmentManager = fragmentManager;
	}

	@Override
	public int getViewLayout() {
		// TODO Auto-generated method stub
		return R.layout.fragment_home;
	}

	@Override
	public void initView() {
		// TODO Auto-generated method stub
		pageTitle = (CustomViewPageTitle) getObject(R.id.home_viewpage_title);
		viewPage = (CustomViewPager) getObject(R.id.home_viewpage);

		pageTitle.setRectColor(0xffcccccc);
		pageTitle.setRectHeight(5);
		pageTitle.setRectBackground(0xffd9ab6f);
		pageTitle.setTitleBackground(0xffd9ab6f);
		pageTitle.setTitleSelectColor(0xffffffff);
		pageTitle.setTitleNomalColor(0xffcccccc);
		pageTitle.setTitleNomal(16);
		pageTitle.setTitleSelect(18);

		hotFragment = new ListFragment();
		allFragment = new ListFragment();

		hotLable = getString(R.string.home_title_lable_hot);
		allLable = getString(R.string.home_title_lable_all);

		viewPage.addPage(hotLable, hotFragment, fragmentManager);
		viewPage.addPage(allLable, allFragment, fragmentManager);

		viewPage.setTitleView(pageTitle);
		viewPage.notifyDataSetChanged();

		pageTitle.setSelectListner(new SelectListener() {

			@Override
			public void onSelected(int select) {
				// TODO Auto-generated method stub
				viewPage.setCurrentPage(select);
			}
		});

	}

}
