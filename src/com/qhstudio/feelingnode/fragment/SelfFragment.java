package com.qhstudio.feelingnode.fragment;

import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.adapter.SelfGrid;
import com.qhstudio.feelingnode.base.BaseFragment;
import com.qhstudio.feelingnode.bean.User;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

public class SelfFragment extends BaseFragment {
	private GridView grid;
	SelfGrid selfGrid;
	Button settings;
	TextView userName;

	@Override
	public int getViewLayout() {
		// TODO Auto-generated method stub
		return R.layout.fragment_self;
	}

	@Override
	public void initView() {
		// TODO Auto-generated method stub
		grid = (GridView) getObject(R.id.self_grid);
		selfGrid = new SelfGrid(getActivity());
		grid.setAdapter(selfGrid);
		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				selfGrid.getObject(arg2).RunAction();
			}
		});

		userName = (TextView) getObject(R.id.user_name);
		setUserInfo();
	}
	
	/**
	 * 设置显示信息
	 */
	void setUserInfo() {
		if (User.getInstance() == null)
			return;

		userName.setText(User.getInstance().getBean().getName());
	}
}
