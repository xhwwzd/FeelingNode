package com.qhstudio.feelingnode.fragment;

import com.qhstudio.feelingnode.ArticleDetail;
import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.adapter.ArticleAdapter;
import com.qhstudio.feelingnode.base.BaseFragment;
import com.qhstudio.feelingnode.tools.ActivityResources;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * 列表
 * 
 * @author xhwwzd
 * 
 */
public class ListFragment extends BaseFragment {

	ListView listView;
	ArticleAdapter articleAdapter;

	@Override
	public int getViewLayout() {
		// TODO Auto-generated method stub
		return R.layout.fragment_list;
	}

	@Override
	public void initView() {
		// TODO Auto-generated method stub
		listView = (ListView) getObject(R.id.fragment_list);
		articleAdapter = new ArticleAdapter(getActivity());
		listView.setAdapter(articleAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				ActivityResources.toActivity(getActivity(), ArticleDetail.class, "info",
						articleAdapter.getObject(position));
				
			}
		});
		
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		articleAdapter.notifyDataSetChanged();
		
		Log.e("info", "start-----"+articleAdapter.getCount());
	}

}
