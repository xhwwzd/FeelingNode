package com.qhstudio.feelingnode;

import com.qhstudio.feelingnode.base.BaseActivity;
import com.qhstudio.feelingnode.bean.WebParam;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

public class WebPlayer extends BaseActivity {

	TextView titleView;
	String STtitle;
	String SURL = ""; // "file:///android_asset/PoolNote/about.html"; //关于
	private Boolean webTitle = false;
	private WebView webView;
	private ProgressBar webProgress;

	WebParam webParam;
	String ErroUrl = "file:///android_asset/PoolNote/err.html";
	public static String WebKey = "WebParam";

	@Override
	public void start() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.activity_web_player;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		setNoti();
		initData();
		initView();
		loadUril(SURL);
	}

	void initData() {
		STtitle = getTitle().toString();

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			webParam = (WebParam) bundle.getSerializable(WebKey);
		}

		if (webParam != null) {
			STtitle = webParam.getTitle();
			SURL = webParam.getUrl();
			if (webParam.getErro() != null && webParam.getErro().length() > 0) {
				ErroUrl = webParam.getErro();
			}

		}
	}

	void initView() {

		titleView = getTextView(R.id.activity_title);
		titleView.setText(STtitle);

		webView = (WebView) findViewById(R.id.view_web);
		webProgress = (ProgressBar) findViewById(R.id.web_progress);
		setWebView();
	}

	/**
	 * 初始化web内容
	 */
	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	private void setWebView() {
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setAppCacheEnabled(false);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setUseWideViewPort(true);
		webSettings.setPluginState(PluginState.ON);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				// TODO Auto-generated method stub
				super.onPageFinished(view, url);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				// TODO Auto-generated method stub
				super.onReceivedError(view, errorCode, description, failingUrl);
				String data = "Page NO FOUND！";
				view.loadUrl("javascript:document.body.innerHTML=\"" + data + "\"");

			}

			@SuppressLint("NewApi")
			@Override
			public void onReceivedHttpError(WebView view, WebResourceRequest request,
					WebResourceResponse errorResponse) {
				// TODO Auto-generated method stub
				super.onReceivedHttpError(view, request, errorResponse);
				String data = "Page NO FOUND！";
				view.loadUrl("javascript:document.body.innerHTML=\"" + data + "\"");
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				view.loadUrl(url);
				return true;
			}

		});

		webView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				// TODO Auto-generated method stub

				if (webView.isClickable())
					webView.setClickable(false);

				super.onProgressChanged(view, newProgress);
				webProgress.setProgress(newProgress);

				if (newProgress > 99) {
					webProgress.setVisibility(View.GONE);
				} else {
					webProgress.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onReceivedTitle(WebView view, String title) {
				// TODO Auto-generated method stub
				super.onReceivedTitle(view, title);

				if (webTitle) {
					if (titleView != null) {
						if (title.length() > 0)
							titleView.setText(title);
						else
							titleView.setText(STtitle);
					}
				}
			}
		});

		webView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loadUril(SURL);
				Log.e("info", "点击View");
			}
		});
	}

	void loadUril(String url) {
		if (url.length() < 1) {
			webView.loadUrl(ErroUrl);
			return;
		}
		webView.loadUrl(url);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// webView.destroy();
	}

}
