package com.qhstudio.feelingnode.views;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/***
 * 自定义viewpager
 * 
 * @author xhwwzd
 * 
 */
public class CustomViewPager extends ViewPager {

	Context context;
	List<PageView> pages = new ArrayList<PageView>();
	int currentPage = 0;
	BasePage basePage;
	FragmentPage fragmentPage;
	FragmentManager fm;

	CustomViewPageTitle pageTitle;

	private boolean isScrollable = true;
	private boolean smoothScroll = true;

	enum Type {
		Fragment, View
	}

	Type type = Type.View;

	public CustomViewPager(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		initView(context);
	}

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initView(context);
	}

	/**
	 * 初始化内容
	 */
	void initView(Context context) {
		this.context = context;
	}

	/**
	 * 添加内容
	 * 
	 * @param title
	 * @param view
	 */
	public void addPage(String title, View view) {
		PageView page = new PageView();
		page.page = view;
		page.pageTile = title;
		pages.add(page);
		type = Type.View;
	}

	/**
	 * 添加内容
	 * 
	 * @param title
	 * @param layout
	 */
	public void addPage(String title, int layout) {
		PageView page = new PageView();
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(layout, null);
		page.page = view;
		page.pageTile = title;
		pages.add(page);
		type = Type.View;
	}

	/**
	 * 添加Fragment
	 * 
	 * @param title
	 * @param fragment
	 */

	public void addPage(String title, Fragment fragment, FragmentManager fm) {
		PageView page = new PageView();
		page.pageTile = title;
		page.fragment = fragment;
		pages.add(page);

		if (this.fm == null)
			this.fm = fm;

		type = Type.Fragment;
	}

	/**
	 * 设置标题
	 * 
	 * @param title
	 */
	public void setTitleView(CustomViewPageTitle title) {
		this.pageTitle = title;
	}

	/**
	 * 更新
	 */
	public void notifyDataSetChanged() {
		this.removeAllViews();
		addViewPager(pages);
		currentPage = 0;
		String[] titles = new String[pages.size()];
		for (int i = 0; i < titles.length; i++) {
			titles[i] = pages.get(i).pageTile;
		}

		if (pageTitle != null) {
			pageTitle.setTile(titles);
		}
	}

	public void setCurrentPage(int page) {
		currentPage = page;
		this.setCurrentItem(page);
	}

	void notiCurrentPage() {
		this.setCurrentItem(currentPage);
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		if (isScrollable)
			return super.onTouchEvent(arg0);
		else
			return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		if (isScrollable)
			return super.onInterceptTouchEvent(arg0);
		else
			return false;
	}

	@Override
	public void setCurrentItem(int item, boolean smoothScroll) {
		super.setCurrentItem(item, smoothScroll);
	}

	@Override
	public void setCurrentItem(int item) {
		super.setCurrentItem(item, isScrollable);
	}

	public void setScrollable(boolean isScrollable) {
		this.isScrollable = isScrollable;
	}

	public boolean isSmoothScroll() {
		return smoothScroll;
	}

	public void setSmoothScroll(boolean smoothScroll) {
		this.smoothScroll = smoothScroll;
	}

	/**
	 * 添加内容
	 * 
	 * @param views
	 */
	void addViewPager(List<PageView> pages) {
		this.setLayoutParams(getFall());
		setPage(this);
	}

	/**
	 * 
	 * @param pager
	 *            需要添加的视图
	 * @param drawable
	 *            视图内容
	 */
	void setPage(ViewPager pager) {

		switch (type) {
		case View:
			basePage = new BasePage(pages);
			pager.setAdapter(basePage);
			break;
		case Fragment:
			fragmentPage = new FragmentPage(fm, pages);
			pager.setAdapter(fragmentPage);
			break;
		}

		pager.addOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				currentPage = arg0;
				if (pageTitle != null) {
					pageTitle.setSelect(currentPage);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				if (pageTitle != null && pageTitle.pageRect != null) {
					pageTitle.pageRect.refRect(arg0, arg2);
				}
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	android.widget.LinearLayout.LayoutParams getFall() {
		android.widget.LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		return lp;
	}

	/**
	 * 基本类型
	 * 
	 * @author xhwwzd
	 * 
	 */
	public class BasePage extends PagerAdapter {
		List<PageView> views;

		public BasePage(List<PageView> list) {
			// TODO Auto-generated constructor stub
			this.views = list;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public int getCount() {
			if (views == null)
				return 0;
			return views.size();
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView(views.get(position).page);
		}

		@Override
		public Object instantiateItem(View container, int position) {
			((ViewPager) container).addView(views.get(position).page);
			return views.get(position).page;
		}
	}

	/**
	 * Fragmengt
	 * 
	 * @author xhwwzd
	 * 
	 */
	public class FragmentPage extends FragmentPagerAdapter {
		private List<PageView> mFragments;

		public FragmentPage(FragmentManager fm, List<PageView> list) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.mFragments = list;
		}

		public void setList(List<PageView> list) {
			this.mFragments = list;
		}

		@Override
		public Fragment getItem(int arg0) {
			// TODO Auto-generated method stub
			return mFragments.get(arg0).fragment;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			if (mFragments == null)
				return 0;
			return mFragments.size();
		}
	}

	/**
	 * 内容配置
	 * 
	 * @author xhwwzd
	 * 
	 */
	public class PageView {
		String pageTile = "";
		int icon = -1;
		View page;
		Fragment fragment;
	}
}
