package com.qhstudio.feelingnode.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 标题栏
 * 
 * @author xhwwzd
 *
 */
public class CustomViewPageTitle extends RelativeLayout {

	Context context;
	// int width;
	// int height;

	LinearLayout titleLayout;
	public PageRect pageRect;
	TextView[] titleTxt;

	int rectHeight = 10;
	int rectBackgroundColor = 0xff123456;
	int rectColor = 0xff1abd9d;
	boolean titletVisable = true;

	int titleBackgoundColor = 0xff123456;
	int titleSelectColor = 0xff1abd9d;

	int titleTextColorNomal = 0xffeeeeee;
	int titleTextColorSelect = -1;
	int titleTextSizeNomal = 16;
	int titleTextSizeSelect = 18;
	boolean rectVisable = true;

	SelectListener selectListener;

	public CustomViewPageTitle(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		initView(context);
	}

	public CustomViewPageTitle(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initView(context);
	}

	@SuppressLint("NewApi")
	public CustomViewPageTitle(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		initView(context);
	}

	@SuppressLint("NewApi")
	public CustomViewPageTitle(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		initView(context);
	}

	void initView(Context context) {
		this.context = context;
		// ViewTreeObserver vto = this.getViewTreeObserver();
		// vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
		// @SuppressWarnings("deprecation")
		// @Override
		// public void onGlobalLayout() {
		// getViewTreeObserver().removeGlobalOnLayoutListener(this);
		// height = getHeight();
		// width = getWidth();
		// configView();
		// }
		// });
		// configView();

	}

	void configView() {
		String[] title = { "a", "b", "c" };
		addTile(title);
		addLine(3);
	}

	public void setTile(String[] title) {
		addTile(title);
		addLine(title.length);
		setSelect(0);
	}
	

	public void setTile(Tab[] title) {
		addTile(title);
		addLine(title.length);
		setSelect(0);
	}


	public void setSelectListner(SelectListener select) {
		this.selectListener = select;
	}

	/**
	 * 添加标题
	 * 
	 * @param titles
	 */
	void addTile(String... titles) {
		titleLayout = new LinearLayout(context);
		titleLayout.setOrientation(LinearLayout.HORIZONTAL);
		titleLayout.setLayoutParams(getFall());
		titleLayout.setBackgroundColor(titleBackgoundColor);

		this.addView(titleLayout);
		titleTxt = new TextView[titles.length];
		for (int i = 0; i < titles.length; i++) {
			titleTxt[i] = new TextView(context);
			titleTxt[i].setLayoutParams(getFall(1.0f));
			titleLayout.addView(titleTxt[i]);
			titleTxt[i].setText(titles[i]);
			titleTxt[i].setTextSize(titleTextSizeNomal);
			titleTxt[i].setTextColor(titleTextColorNomal);
			titleTxt[i].setGravity(Gravity.CENTER);
			titleTxt[i].setTag(i);
			titleTxt[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int s = Integer.parseInt(String.valueOf(v.getTag()));
					if (selectListener != null) {
						selectListener.onSelected(s);
					}
				}
			});
		}
	}
	
	void addTile(Tab... titles) {
		titleLayout = new LinearLayout(context);
		titleLayout.setOrientation(LinearLayout.HORIZONTAL);
		titleLayout.setLayoutParams(getFall());
		titleLayout.setBackgroundColor(titleBackgoundColor);

		this.addView(titleLayout);
		titleTxt = new TextView[titles.length];
		for (int i = 0; i < titles.length; i++) {
			titleTxt[i] = new TextView(context);
			titleTxt[i].setLayoutParams(getFall(1.0f));
			titleLayout.addView(titleTxt[i]);
			titleTxt[i].setText(titles[i].title);
			titleTxt[i].setTextSize(titleTextSizeNomal);
			titleTxt[i].setTextColor(titleTextColorNomal);
			titleTxt[i].setGravity(Gravity.CENTER);
			titleTxt[i].setTag(i);
			titleTxt[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int s = Integer.parseInt(String.valueOf(v.getTag()));
					if (selectListener != null) {
						selectListener.onSelected(s);
					}
				}
			});
		}
	}

	/**
	 * 添加线条
	 */
	void addLine(int size) {
		pageRect = new PageRect(context);
		pageRect.setPageCont(size);
		pageRect.setBackgroundColor(rectBackgroundColor);
		pageRect.setColor(rectColor);
		RelativeLayout.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		lp.height = rectHeight;
		lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		pageRect.setLayoutParams(lp);
		this.addView(pageRect);
	}

	public void setSelect(int i) {
		if (titleTxt == null)
			return;
		for (int t = 0; t < titleTxt.length; t++) {
			if (t == i) {
				titleTxt[t].setTextSize(titleTextSizeSelect);
				titleTxt[t].setTextColor(titleSelectColor);
			} else {
				titleTxt[t].setTextSize(titleTextSizeNomal);
				titleTxt[t].setTextColor(titleTextColorNomal);
			}
		}
	}

	/**
	 * 显示滑动背景色
	 * 
	 * @param color
	 */
	public void setRectBackground(int color) {
		this.rectBackgroundColor = color;
	}

	/**
	 * 显示滑动颜色
	 * 
	 * @param color
	 */
	public void setRectColor(int color) {
		this.rectColor = color;
	}

	/**
	 * 显示滑动高度
	 * 
	 * @param height
	 */
	public void setRectHeight(int height) {
		this.rectHeight = height;
	}

	/**
	 * 显示滑动
	 * 
	 * @param show
	 */
	public void setRectVisable(boolean show) {
		this.rectVisable = show;
	}

	/**
	 * 显示标题
	 * 
	 * @param show
	 */
	public void setTitleVisable(boolean show) {
		this.titletVisable = show;
	}

	/**
	 * 选中字体大小
	 * 
	 * @param size
	 */
	public void setTitleSelect(int size) {
		this.titleTextSizeSelect = size;
	}

	/**
	 * 默认字体大小
	 * 
	 * @param size
	 */
	public void setTitleNomal(int size) {
		this.titleTextSizeNomal = size;
	}

	/**
	 * 标题背景色
	 * 
	 * @param color
	 */
	public void setTitleBackground(int color) {
		this.titleBackgoundColor = color;
	}

	/**
	 * 选中颜色
	 * 
	 * @param color
	 */
	public void setTitleSelectColor(int color) {
		this.titleSelectColor = color;
	}

	/**
	 * 默认颜色
	 * 
	 * @param color
	 */
	public void setTitleNomalColor(int color) {
		this.titleTextColorNomal = color;
	}

	LayoutParams getFall() {
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		return lp;
	}

	LinearLayout.LayoutParams getFall(float weigth) {
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, weigth);
		return lp;
	}

	class PageRect extends View {
		Paint paint;
		Paint bgPaint;

		float viewWidth = 0;
		float screenWidth = 0;
		int cont = 4;
		float rectX, rectY, rectW, rectH;

		public PageRect(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
			init(context);
		}

		public PageRect(Context context, AttributeSet attrs) {
			super(context, attrs);
			// TODO Auto-generated constructor stub
			init(context);
		}

		public PageRect(Context context, AttributeSet attrs, int defStyleAttr) {
			super(context, attrs, defStyleAttr);
			// TODO Auto-generated constructor stub
			init(context);
		}

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			// TODO Auto-generated method stub
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}

		/**
		 * 初始化内容
		 * 
		 * @param context
		 */
		@SuppressWarnings("deprecation")
		private void init(Context context) {
			rectX = 0;
			rectY = 0;
			rectW = viewWidth / cont;
			rectH = 100;

			paint = new Paint();
			paint.setAntiAlias(true);
			paint.setColor(0XFFFF0000);
			paint.setStyle(Paint.Style.FILL);

			bgPaint = new Paint();
			bgPaint.setAntiAlias(true);
			bgPaint.setColor(0xFFFFFFFF);
			bgPaint.setStyle(Paint.Style.FILL);

			WindowManager screen = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			Display display = screen.getDefaultDisplay();
			screenWidth = display.getWidth();

			ViewTreeObserver vto = this.getViewTreeObserver();
			vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					getViewTreeObserver().removeGlobalOnLayoutListener(this);
					// height = getHeight();
					// viewWidth = getWidth();
					setViewWidth(getWidth());

				}
			});

		}

		/**
		 * 设置页面数量
		 * 
		 * @param page
		 */
		public void setPageCont(int page) {
			this.cont = page;
			rectW = viewWidth / cont;
			invalidate();
		}

		/**
		 * 设置宽度
		 * 
		 * @param width
		 */
		void setViewWidth(int width) {
			this.viewWidth = width;
			invalidate();
		}

		/**
		 * 更新界面
		 */
		public void updateView() {
			this.invalidate();
		}

		/**
		 * 设置颜色
		 * 
		 * @param color
		 */
		public void setColor(int color) {
			paint.setColor(color);
			invalidate();
		}

		/**
		 * 设置背景颜色
		 */
		public void setBackgroundColor(int color) {
			bgPaint.setColor(color);
			invalidate();
		}

		/**
		 * 设置滑动
		 * 
		 * @param page
		 * @param position
		 */
		public void refRect(int page, int position) {
			// Log.e("info", "page-" + page + ">>>>" + position + ",viewWidth,"
			// + viewWidth);
			rectX = (position / screenWidth) * (viewWidth / cont) + (viewWidth / cont) * page;
			// rectW = rectX + screenWidth / cont;
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			// TODO Auto-generated method stub
			super.onDraw(canvas);
			viewWidth = this.getWidth();
			rectW = rectX + viewWidth / cont;
			canvas.drawRect(0, 0, viewWidth, viewWidth, bgPaint); // 绘制背景色
			canvas.drawRect(rectX, rectY, rectW, rectH, paint); // 绘制矩形
		}
	}
	/**
	 * 标题
	 * 
	 * @author xhwwzd
	 *
	 */
	public class Tab {
		public String title;
		public int icon;
	}
	public interface SelectListener {
		public void onSelected(int select);
	}
}
