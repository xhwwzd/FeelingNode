package com.qhstudio.feelingnode.views;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.view.View;

public class ViewSelector {

	int[] nomalState = { -android.R.attr.state_focused, -android.R.attr.state_selected, -android.R.attr.state_pressed };
	int[] focusedState = { android.R.attr.state_focused, -android.R.attr.state_selected,
			-android.R.attr.state_pressed };

	int[] selectedState = { android.R.attr.state_selected, android.R.attr.state_pressed };
	int[] noSelectedState = { -android.R.attr.state_selected, -android.R.attr.state_pressed };

	int[] nocheckState = { -android.R.attr.state_selected, -android.R.attr.state_checked };
	int[] checkState = { android.R.attr.state_selected, android.R.attr.state_checked };

	int[] pressedState = { android.R.attr.state_selected, android.R.attr.state_pressed };
	Context context;

	public ViewSelector(Context context) {
		this.context = context;
	}

	@SuppressWarnings("deprecation")
	public void setFocused(Context context, View view, int nomal, int focuse) {
		StateListDrawable drawable = new StateListDrawable();
		// Non focused states
		drawable.addState(nocheckState, context.getResources().getDrawable(nomal));

		// Focused states
		drawable.addState(focusedState, context.getResources().getDrawable(focuse));

		// Pressed
		drawable.addState(pressedState, context.getResources().getDrawable(focuse));

		view.setBackgroundDrawable(drawable);
	}

	@SuppressWarnings("deprecation")
	public void setCheck(Context context, View view, int nomal, int check) {
		StateListDrawable drawable = new StateListDrawable();
		drawable.addState(nocheckState, context.getResources().getDrawable(nomal));
		drawable.addState(checkState, context.getResources().getDrawable(check));
		view.setBackgroundDrawable(drawable);
	}
}
