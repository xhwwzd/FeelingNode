package com.qhstudio.feelingnode.views;

import java.util.ArrayList;
import java.util.List;

import com.qhstudio.feelingnode.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * @author wo2app
 * @date 创建时间：2015年8月5日 下午7:31:30
 * @version 1.0
 * @parameter
 * @since
 * @return
 */

public class ItemGroup extends LinearLayout {

	private Context context;
	private List<View> itemList = new ArrayList<View>();

	public ItemGroup(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public ItemGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	@SuppressLint("NewApi")
	public ItemGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public void addItem(final onItemClick click) {
		LayoutInflater inflater = LayoutInflater.from(this.context);
		final View view = inflater.inflate(R.layout.view_menu_item, null);
		this.addView(view);
		itemList.add(view);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (click != null) {
					click.itemClick(itemList.indexOf(view), "");
				}
			}
		});
	}

	public void addItem(int icon, final onItemClick click) {
		LayoutInflater inflater = LayoutInflater.from(this.context);
		final View view = inflater.inflate(R.layout.view_menu_item, null);
		this.addView(view);
		itemList.add(view);
		ImageView img = (ImageView) view.findViewById(R.id.item_img);
		img.setImageResource(icon);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (click != null) {
					click.itemClick(itemList.indexOf(view), "");
				}
			}
		});
	}

	public void addItem(final String txt, final onItemClick click) {
		LayoutInflater inflater = LayoutInflater.from(this.context);
		final View view = inflater.inflate(R.layout.view_menu_item, null);
		this.addView(view);
		itemList.add(view);
		TextView lable = (TextView) view.findViewById(R.id.item_txt);
		lable.setText(txt);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (click != null) {
					click.itemClick(itemList.indexOf(view), txt);
				}
			}
		});
	}

	public void addItem(final String txt, String info, final onItemClick click) {
		LayoutInflater inflater = LayoutInflater.from(this.context);
		final View view = inflater.inflate(R.layout.view_menu_item, null);
		this.addView(view);
		itemList.add(view);
		ImageView img = (ImageView) view.findViewById(R.id.item_img);
		img.setVisibility(View.GONE);
		TextView lable = (TextView) view.findViewById(R.id.item_txt);
		lable.setText(txt);
		TextView itemInfo = (TextView) view.findViewById(R.id.item_info);
		itemInfo.setText(info);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (click != null) {
					click.itemClick(itemList.indexOf(view), txt);
				}
			}
		});
	}

	public void addItem(int icon, final String txt, final onItemClick click) {
		LayoutInflater inflater = LayoutInflater.from(this.context);
		final View view = inflater.inflate(R.layout.view_menu_item, null);
		this.addView(view);
		itemList.add(view);
		ImageView img = (ImageView) view.findViewById(R.id.item_img);
		img.setImageResource(icon);
		TextView lable = (TextView) view.findViewById(R.id.item_txt);
		lable.setText(txt);
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (click != null) {
					click.itemClick(itemList.indexOf(view), txt);
				}
			}
		});

	}

	public void addItem(int icon, final String txt, String info, final onItemClick click) {
		LayoutInflater inflater = LayoutInflater.from(this.context);
		final View view = inflater.inflate(R.layout.view_menu_item, null);
		this.addView(view);
		itemList.add(view);
		ImageView img = (ImageView) view.findViewById(R.id.item_img);
		img.setImageResource(icon);
		TextView lable = (TextView) view.findViewById(R.id.item_txt);
		lable.setText(txt);
		TextView itemInfo = (TextView) view.findViewById(R.id.item_info);
		itemInfo.setText(info);

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (click != null) {
					click.itemClick(itemList.indexOf(view), txt);
				}
			}
		});
	}

	/**
	 * 添加线条
	 * 
	 * @param left
	 * @param right
	 * @param top
	 * @param bottom
	 */
	public void addLine(int left, int right, int top, int bottom, int height) {

		View line = new View(this.context);
		LinearLayout.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		line.setBackgroundResource(R.color.line_bg);
		line.setMinimumHeight(height);
		lp.setMargins(left, top, right, bottom);
		this.addView(line, lp);
	}

	/**
	 * 添加线条
	 * 
	 * @param left
	 * @param right
	 * @param top
	 * @param bottom
	 * @param height
	 * @param bg
	 */
	public void addLine(int left, int right, int top, int bottom, int height, int bg) {
		LinearLayout layout = new LinearLayout(this.context);
		layout.setOrientation(HORIZONTAL);

		LinearLayout.LayoutParams lplayout = new LayoutParams(LayoutParams.MATCH_PARENT, height);
		lplayout.setMargins(0, top, 0, bottom);

		LinearLayout.LayoutParams l1p = new LayoutParams(left, height);
		View l1 = new View(this.context);
		l1.setBackgroundResource(bg);

		LinearLayout.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		View l2 = new View(this.context);
		l2.setMinimumHeight(height);
		l2.setBackgroundResource(R.color.line_bg);

		layout.addView(l1, l1p);
		layout.addView(l2, lp);

		this.addView(layout, lplayout);
	}

	/**
	 * 更新数据
	 * 
	 * @param position
	 * @param icon
	 * @param lable
	 * @param info
	 */
	public void upDataView(int position, int icon, String lable, String info) {
		View view = itemList.get(position);

		if (icon > 0) {
			ImageView img = (ImageView) view.findViewById(R.id.item_img);
			img.setImageResource(icon);
		}
		if (lable != null) {
			TextView txt1 = (TextView) view.findViewById(R.id.item_txt);
			txt1.setText(lable);
		}
		if (info != null) {
			TextView itemInfo = (TextView) view.findViewById(R.id.item_info);
			itemInfo.setText(info);
		}
	}

	/**
	 * 事件处理
	 * 
	 * @author xhwwzd
	 *
	 */
	public interface onItemClick {
		void itemClick(int click, String lable);
	}

}
