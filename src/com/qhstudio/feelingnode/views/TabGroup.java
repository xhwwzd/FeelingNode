package com.qhstudio.feelingnode.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * 
 * TabGroup tab=(TabGroup)getObject(R.id.tabGroup); tab.TextColor=
 * R.color.menu_text_color; tab.TextSize=14;
 * tab.addTab(R.drawable.home_nomal,R.drawable.home_active,"Home");
 * tab.addTab(R.drawable.tab_home,"Pef"); tab.addTab(R.drawable.tab_home,"Set");
 * tab.addListener(new CheckListener() {
 * 
 * @Override public void checkListener(int postion) { // TODO Auto-generated
 *           method stub Log.e("info", postion+">>>>"); } }); tab.commit();
 * @author xhwwzd
 *
 */

public class TabGroup extends RadioGroup {

	Context context;
	public int TextColor = -1;
	public float TextSize = 12;
	public int panddingTop = 5;
	public int panddingBottom = 3;
	public int panddingLeft = 0;
	public int panddingRight = 0;

	List<RadioButton> buttons = new ArrayList<RadioButton>();

	int[] nomalState = { -android.R.attr.state_checked };

	int[] checkState = { android.R.attr.state_checked };

	public enum DrawDir {
		top, botton, left, right, background
	};

	public DrawDir drawDir = DrawDir.top;
	public CheckListener checkListener;

	public TabGroup(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public TabGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	@SuppressWarnings("deprecation")
	public void addTab(int drawable, String lable) {
		RadioButton button = new RadioButton(context);
		button.setText(lable);
		button.setTextSize(TextSize);
		button.setGravity(Gravity.CENTER);

		Drawable top = ContextCompat.getDrawable(context, drawable);
		switch (drawDir) {
		case top:
			button.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case botton:
			button.setCompoundDrawablesWithIntrinsicBounds(null, null, null, top);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case left:
			button.setCompoundDrawablesWithIntrinsicBounds(top, null, null, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case right:
			button.setCompoundDrawablesWithIntrinsicBounds(null, null, top, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case background:
			button.setButtonDrawable(top);
			break;
		}
		button.setBackgroundDrawable(null);
		button.setPadding(panddingLeft, panddingTop, panddingRight, panddingBottom);
		this.addView(button, getFall(1));
		if (TextColor != -1) {
			Resources resource = (Resources) context.getResources();
			ColorStateList csl = (ColorStateList) resource.getColorStateList(TextColor);
			if (csl != null) {
				button.setTextColor(csl);
			}
		}
		this.invalidate();
		button.setId(buttons.size());
		buttons.add(button);
	}

	@SuppressWarnings("deprecation")
	public void addTab(int nomal, int check, String lable) {
		RadioButton button = new RadioButton(context);
		button.setText(lable);
		button.setTextSize(TextSize);
		button.setGravity(Gravity.CENTER);

		StateListDrawable drawable = new StateListDrawable();
		drawable.addState(nomalState, ContextCompat.getDrawable(context, nomal));
		drawable.addState(checkState, ContextCompat.getDrawable(context, check));

		switch (drawDir) {
		case top:
			button.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case botton:
			button.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case left:
			button.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case right:
			button.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case background:
			button.setButtonDrawable(drawable);
			break;
		}
		button.setPadding(panddingLeft, panddingTop, panddingRight, panddingBottom);
		button.setBackgroundDrawable(null);
		
		this.addView(button, getFall(1));
		if (TextColor != -1) {
			Resources resource = (Resources) context.getResources();
			ColorStateList csl = (ColorStateList) resource.getColorStateList(TextColor);
			if (csl != null) {
				button.setTextColor(csl);
			}
		}
		this.invalidate();
		button.setId(buttons.size());
		buttons.add(button);
	}

	@SuppressWarnings("deprecation")
	public void addTab(int nomal, int check, String lable, int id) {
		RadioButton button = new RadioButton(context);
		button.setText(lable);
		button.setTextSize(TextSize);
		button.setGravity(Gravity.CENTER);

		StateListDrawable drawable = new StateListDrawable();
		drawable.addState(nomalState, ContextCompat.getDrawable(context, nomal));
		drawable.addState(checkState, ContextCompat.getDrawable(context, check));

		switch (drawDir) {
		case top:
			button.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case botton:
			button.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case left:
			button.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case right:
			button.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
			button.setButtonDrawable(android.R.color.transparent);
			break;
		case background:
			button.setButtonDrawable(drawable);
			break;
		}
		button.setBackgroundDrawable(null);
		button.setPadding(panddingLeft, panddingTop, panddingRight, panddingBottom);
		this.addView(button, getFall(1));
		if (TextColor != -1) {
			Resources resource = (Resources) context.getResources();
			ColorStateList csl = (ColorStateList) resource.getColorStateList(TextColor);
			if (csl != null) {
				button.setTextColor(csl);
			}
		}
		this.invalidate();
		button.setId(id);
		buttons.add(button);
	}

	public void setChecked(int i, Boolean check) {
		if (buttons == null || i > buttons.size())
			return;
		buttons.get(i).setChecked(check);
	}

	public void addListener(CheckListener checkListener) {
		this.checkListener = checkListener;
	}

	public void commit() {
		this.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if (checkListener != null) {
					checkListener.checkListener(checkedId);
				}
			}
		});
	}

	LayoutParams getFall(float weigth) {
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, weigth);
		lp.gravity = Gravity.CENTER;
		return lp;
	}

	public interface CheckListener {
		void checkListener(int postion);
	}
}
