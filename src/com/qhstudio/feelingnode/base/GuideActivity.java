package com.qhstudio.feelingnode.base;

import java.util.ArrayList;

import com.qhstudio.feelingnode.MainActivity;
import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.data.ApplicationConfig;
import com.qhstudio.feelingnode.data.UserPrefs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author wo2app
 * @email wo2app@qq.com
 * @time 2014年12月7日
 * @version V1.0.0
 * @function 引导页面，首次启动使用
 */
public class GuideActivity extends Activity {

	private ViewPager viewPager;// viewpager
	private Button skip;
	private TextView guidText; // 设置文本
	private int allPage = ApplicationConfig.GuideDrawable.length;
	private boolean viewable = false;
	private String Key = "first";
	UserPrefs config ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE); // 去掉标题栏

		if (ApplicationConfig.SplashFallScreen) {
			this.getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);// 去掉信息栏
		}
		config = new UserPrefs(this, "System");
		
		if (!ApplicationConfig.UseGuide) {
			toParent();
			return;
		} else if (config.getBoolean(Key, false)) {
			toParent();
			return;
		}

		setContentView(R.layout.activity_guide);

		initView();
		setPage(viewPager, ApplicationConfig.GuideDrawable);
		setText();
	}

	/**
	 * 初始化界面内容
	 */
	private void initView() {
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		skip = (Button) findViewById(R.id.guid_skip);
		guidText = (TextView) findViewById(R.id.guide_txt);

		skip.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				toParent();
			}
		});

	}

	/**
	 * 
	 * @param pager
	 *            需要添加的视图
	 * @param drawable
	 *            视图内容
	 */
	private void setPage(ViewPager pager, int[] drawable) {

		LayoutInflater inflater = LayoutInflater.from(this);

		final ArrayList<View> views = new ArrayList<View>();
		for (int i = 0; i < drawable.length; i++) {
			View view = inflater.inflate(R.layout.activity_guide_item, null);
			ImageView image = (ImageView) view.findViewById(R.id.guide_item);
			image.setBackgroundResource(drawable[i]);
			views.add(view);
		}

		PagerAdapter mPagerAdapter = new PagerAdapter() {

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}

			@Override
			public int getCount() {
				return views.size();
			}

			@Override
			public void destroyItem(View container, int position, Object object) {
				((ViewPager) container).removeView(views.get(position));

			}

			@Override
			public Object instantiateItem(View container, int position) {
				((ViewPager) container).addView(views.get(position));

				return views.get(position);
			}
		};
		pager.setAdapter(mPagerAdapter);
		pager.addOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				setText();
			}
		});

	}

	/**
	 * 进入应用
	 */
	private void toParent() {
		
		config.setBoolean(Key, true);

		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}

	/*
	 * 设置页面显示提示页面
	 */
	private void setText() {
		guidText.setText(viewPager.getCurrentItem() + 1 + "/" + allPage);

		if ((viewPager.getCurrentItem() + 1) == allPage) {
			viewable = true;
			setAnimation(viewable, skip);

		}
		if (viewable && (viewPager.getCurrentItem() + 1) != allPage) {
			viewable = false;
			setAnimation(viewable, skip);
		}
	}

	/**
	 * 设置动画
	 * 
	 * @param visible
	 *            显示、隐藏
	 * @param view
	 *            控件
	 */

	private void setAnimation(boolean visible, final View view) {
		TranslateAnimation animation;
		AnimationSet animationSet = new AnimationSet(true);

		if (visible && view.getVisibility() != View.VISIBLE) {
			animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0f,
					Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
					2f, Animation.RELATIVE_TO_SELF, 0f);
			animation.setDuration(500);
			animationSet.addAnimation(animation);
			view.startAnimation(animationSet);
			view.setVisibility(View.VISIBLE);
		}
		if (!visible && view.getVisibility() == View.VISIBLE) {
			animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0f,
					Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
					0f, Animation.RELATIVE_TO_SELF, 2f);
			animation.setDuration(500);
			animationSet.addAnimation(animation);
			view.startAnimation(animationSet);
			animationSet.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					view.setVisibility(View.GONE);
				}
			});
		}
	}

	/**
	 * 屏蔽返回按钮
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		return false;
	}
}
