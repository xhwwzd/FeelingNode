package com.qhstudio.feelingnode.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment {

	public View baseView = null;
	public LayoutInflater inflater;

	public abstract int getViewLayout();

	public abstract void initView();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		baseView = inflater.inflate(getViewLayout(), container, false);
		this.inflater = inflater;
		return baseView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	/**
	 * 获取通用对象
	 * 
	 * @param id
	 * @return
	 */
	protected Object getObject(int id) {
		return getView().findViewById(id);
	}

	public View getView() {
		return baseView;
	}

	public LayoutInflater getInflater() {
		return inflater;
	}
}
