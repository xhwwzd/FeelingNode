package com.qhstudio.feelingnode.base;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public abstract class SuperAdapter<T> extends BaseAdapter {

	List<T> list = new ArrayList<T>();
	Context context;

	public abstract int getLayout();

	public abstract void setChildView(int position, T data, ViewHolder holder);

	public SuperAdapter(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public SuperAdapter(Context context, List<T> list) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.list = list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	public T getObject(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(this.context);
			convertView = inflater.inflate(getLayout(), parent, false);
			viewHolder = new ViewHolder();
			viewHolder.view = convertView;
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		setChildView(position, list.get(position), viewHolder);
		return convertView;
	}

	public static class ViewHolder {
		public View view;

		public Object getObject(int id) {
			return view.findViewById(id);
		}

		public TextView getTextView(int id) {
			return (TextView) view.findViewById(id);
		}

		public Button getButtonView(int id) {
			return (Button) view.findViewById(id);
		}

		public View getChildView(int id) {
			return view.findViewById(id);
		}

	}
}
