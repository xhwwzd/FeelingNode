package com.qhstudio.feelingnode.base;

import java.util.ArrayList;
import java.util.List;

import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.views.SystemBarTintManager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public abstract class BaseFragmentActivity extends FragmentActivity {

	public abstract void start();

	public abstract int getLayout();

	public abstract void initialize();

	public FragmentManager fragmentManager;
	public FragmentTransaction fragmentTransaction;
	private List<Fragment> fragmentList = new ArrayList<Fragment>();

	public BaseFragmentActivity() {
		// TODO Auto-generated constructor stub
		fragmentManager = getSupportFragmentManager();
	}

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		start();
		setContentView(getLayout());
		initialize();
	}

	protected void addFragment(int id, Fragment fragment) {
		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(id, fragment);
		fragmentTransaction.commit();
		fragmentList.add(fragment);
	}

	protected void showFragment(Fragment fragment) {
		if (fragment == null)
			return;

		fragmentTransaction = fragmentManager.beginTransaction();

		for (Fragment temp : fragmentList) {
			fragmentTransaction.hide(temp);
		}

		fragmentTransaction.show(fragment);
		fragmentTransaction.commit();
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	public void setNoti() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			setTranslucentStatus(true);
		}
		SystemBarTintManager tintManager = new SystemBarTintManager(this);
		tintManager.setStatusBarTintEnabled(true);
		tintManager.setStatusBarTintResource(R.color.notice_bg);
		tintManager.checkNavigation(R.color.notice_bg);
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	public void setNoti(int color) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			setTranslucentStatus(true);
		}
		SystemBarTintManager tintManager = new SystemBarTintManager(this);
		tintManager.setStatusBarTintEnabled(true);
		tintManager.setStatusBarTintResource(color);
		tintManager.checkNavigation(color);
	}

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.KITKAT)
	public void setNoti(int color1, int color2) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			setTranslucentStatus(true);
		}
		SystemBarTintManager tintManager = new SystemBarTintManager(this);
		tintManager.setStatusBarTintEnabled(true);
		tintManager.setStatusBarTintResource(color1);
		tintManager.checkNavigation(color2);
	}

	@TargetApi(19)
	private void setTranslucentStatus(boolean on) {
		Window win = getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
		if (on) {
			winParams.flags |= bits;
		} else {
			winParams.flags &= ~bits;
		}
		win.setAttributes(winParams);
	}

	/**
	 * 获取通用对象
	 * 
	 * @param id
	 * @return
	 */
	protected Object getObject(int id) {
		return findViewById(id);
	}

	
	/**
	 * 获取文本
	 * 
	 * @param id
	 * @return
	 */
	protected TextView getTextView(int id) {
		return (TextView) findViewById(id);
	}

	/**
	 * 获取图片
	 * 
	 * @param id
	 * @return
	 */
	protected ImageView getImageView(int id) {
		return (ImageView) findViewById(id);
	}

	/**
	 * 获取按钮
	 * 
	 * @param id
	 * @return
	 */
	protected Button getButton(int id) {
		return (Button) findViewById(id);
	}

	/**
	 * 获取布局
	 * 
	 * @param id
	 * @return
	 */
	protected LinearLayout getLinearLayout(int id) {
		return (LinearLayout) findViewById(id);
	}

	/**
	 * 获取list
	 * 
	 * @param id
	 * @return
	 */
	protected ListView getListView(int id) {
		return (ListView) findViewById(id);
	}

	/**
	 * 获取webview
	 * 
	 * @param id
	 * @return
	 */
	protected WebView getWebView(int id) {
		return (WebView) findViewById(id);
	}

	/**
	 * 获取输入
	 * 
	 * @param id
	 * @return
	 */
	protected EditText getEditText(int id) {
		return (EditText) findViewById(id);
	}

	/**
	 * 获取check
	 * 
	 * @param id
	 * @return
	 */
	protected CheckBox getCheckBox(int id) {
		return (CheckBox) findViewById(id);
	}
}
