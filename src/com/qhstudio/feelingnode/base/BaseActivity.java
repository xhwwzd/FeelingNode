package com.qhstudio.feelingnode.base;

import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.views.SystemBarTintManager;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public abstract class BaseActivity extends Activity {

	public abstract void start();

	public abstract int getLayout();

	public abstract void initialize();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		start();
		setContentView(getLayout());
		initialize();
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	public void setNoti() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			setTranslucentStatus(true);
		}
		SystemBarTintManager tintManager = new SystemBarTintManager(this);
		tintManager.setStatusBarTintEnabled(true);
		tintManager.setStatusBarTintResource(R.color.notice_bg);
		tintManager.checkNavigation(R.color.notice_bg);
	}

	@TargetApi(Build.VERSION_CODES.KITKAT)
	public void setNoti(int color) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			setTranslucentStatus(true);
		}

		SystemBarTintManager tintManager = new SystemBarTintManager(this);
		tintManager.setStatusBarTintEnabled(true);
		tintManager.setStatusBarTintResource(color);
		tintManager.checkNavigation(color);

	}

	@TargetApi(19)
	private void setTranslucentStatus(boolean on) {
		Window win = getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
		if (on) {
			winParams.flags |= bits;
		} else {
			winParams.flags &= ~bits;
		}
		win.setAttributes(winParams);
	}

	/**
	 * 获取通用对象
	 * 
	 * @param id
	 * @return
	 */
	protected Object getObject(int id) {
		return findViewById(id);
	}

	/**
	 * 获取文本
	 * 
	 * @param id
	 * @return
	 */
	protected TextView getTextView(int id) {
		return (TextView) findViewById(id);
	}

	/**
	 * 获取图片
	 * 
	 * @param id
	 * @return
	 */
	protected ImageView getImageView(int id) {
		return (ImageView) findViewById(id);
	}

	/**
	 * 获取按钮
	 * 
	 * @param id
	 * @return
	 */
	protected Button getButton(int id) {
		return (Button) findViewById(id);
	}

	/**
	 * 获取布局
	 * 
	 * @param id
	 * @return
	 */
	protected LinearLayout getLinearLayout(int id) {
		return (LinearLayout) findViewById(id);
	}

	/**
	 * 获取list
	 * 
	 * @param id
	 * @return
	 */
	protected ListView getListView(int id) {
		return (ListView) findViewById(id);
	}

	/**
	 * 获取webview
	 * 
	 * @param id
	 * @return
	 */
	protected WebView getWebView(int id) {
		return (WebView) findViewById(id);
	}

	/**
	 * 获取输入
	 * 
	 * @param id
	 * @return
	 */
	protected EditText getEditText(int id) {
		return (EditText) findViewById(id);
	}

	/**
	 * 获取check
	 * 
	 * @param id
	 * @return
	 */
	protected CheckBox getCheckBox(int id) {
		return (CheckBox) findViewById(id);
	}

	/**
	 * 设置标题
	 * 
	 * @param title
	 * @param txt
	 */
	public void setTitle(String title, TextView txt) {
		if (txt != null && title != null)
			txt.setText(title);
	}
	/**
	 * 
	 * @param title
	 * @param id
	 */
	public void setTitle(String title, int id) {
		TextView txt = (TextView) findViewById(id);
		setTitle(title, txt);
	}
	/**
	 * 
	 * @param txt
	 */
	public void setTitle(TextView txt) {
		String title = getTitle().toString();
		setTitle(title, txt);
	}
	/**
	 * 
	 */
	public void setTitle(int id) {
		TextView txt = (TextView) findViewById(id);
		String title = getTitle().toString();
		setTitle(title, txt);
	}

	/**
	 * 返回
	 * 
	 * @param back
	 */
	public void onBack(View back) {
		onBackPressed();
		finish();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}
}
