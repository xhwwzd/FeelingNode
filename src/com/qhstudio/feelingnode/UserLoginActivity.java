package com.qhstudio.feelingnode;

import com.qhstudio.feelingnode.base.BaseActivity;
import com.qhstudio.feelingnode.tools.ActivityResources;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class UserLoginActivity extends BaseActivity {

	Button titleAction;

	@Override
	public void start() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.activity_login;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		setNoti();
		setTitle(R.id.activity_title);
		titleAction = getButton(R.id.title_action);
		viewAction();
	}

	void viewAction() {
		titleAction.setVisibility(View.VISIBLE);
		titleAction.setBackgroundResource(R.drawable.button_register);
		titleAction.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ActivityResources.toActivity(UserLoginActivity.this, UserRegisterActivity.class); 
			}
		});
	}

}
