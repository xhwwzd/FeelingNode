package com.qhstudio.feelingnode.bean;

public class SelfGridBean {

	public int icon = -1;
	public String lable;
	public ClickAction click;

	public SelfGridBean() {

	}

	public SelfGridBean(int icon, String lable) {
		this.icon = icon;
		this.lable = lable;
	}

	public SelfGridBean(int icon, String lable, ClickAction action) {
		this.icon = icon;
		this.lable = lable;
		this.click = action;
	}

	public void setClick(ClickAction action) {
		this.click = action;
	}

	public void RunAction() {
		if (click != null)
			click.onClick();
	}

	public interface ClickAction {
		public void onClick();
	}
}
