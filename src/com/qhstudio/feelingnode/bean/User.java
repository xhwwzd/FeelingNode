package com.qhstudio.feelingnode.bean;

import java.util.HashMap;
import java.util.Map;

import com.qhstudio.feelingnode.tools.FellingClient;
import com.qhstudio.feelingnode.tools.FellingClient.ClientType;

public class User {

	private static User Instance;
	private UserBean bean;

	public User() {
		Instance = this;
		bean = new UserBean();
	}

	public void Login(String name, String pwd) {
		String url = "";
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", name);
		map.put("pwd", pwd);
		FellingClient client = new FellingClient(url, ClientType.POST, map);
	}

	public void update(String key, String value) {
		String url = "";
		Map<String, String> map = new HashMap<String, String>();
		map.put("session", bean.getSession());
		map.put(key, value);
		FellingClient client = new FellingClient(url, ClientType.POST, map);
	}

	public static User getInstance() {
		return Instance;
	}

	public UserBean getBean() {
		return bean;
	}


}
