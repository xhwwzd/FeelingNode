package com.qhstudio.feelingnode.bean;

import java.io.Serializable;

/**
 * 发表内容
 * 
 * @author xhwwz
 * 
 */
public class ArticleBean implements Serializable  {

	private static final long serialVersionUID = 2504599642461078530L;

	private int id;

	private String title="测试标题";
	private String sender="匿名";
	private String time="一小时前";
	private String article="测试内容咯";
	private int shareCount=0;
	private int talkCount=19;
	private int like=30;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getArticle() {
		return article;
	}
	public void setArticle(String article) {
		this.article = article;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public int getShareCount() {
		return shareCount;
	}
	public void setShareCount(int shareCount) {
		this.shareCount = shareCount;
	}
	public int getTalkCount() {
		return talkCount;
	}
	public void setTalkCount(int talkCount) {
		this.talkCount = talkCount;
	}
	public int getLike() {
		return like;
	}
	public void setLike(int like) {
		this.like = like;
	}

}
