package com.qhstudio.feelingnode.bean;

import java.io.Serializable;

/**
 * 浏览器参数
 * 
 * @author xhwwzd
 *
 */
public class WebParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6667189319944315301L;
	String title = "title";
	String url = "url";
	String erro = "";
	Boolean webTitle = false;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public Boolean getWebTitle() {
		return webTitle;
	}

	public void setWebTitle(Boolean webTitle) {
		this.webTitle = webTitle;
	}

}
