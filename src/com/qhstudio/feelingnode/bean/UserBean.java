package com.qhstudio.feelingnode.bean;

/**
 * 
 * @author xhwwzd
 *
 */
public class UserBean {
	private Boolean isLogin = true;
	private String session = ""; // 临时值
	private String name = "若远帆"; // 昵称
	private String headUrl = ""; // 头像

	public Boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

}
