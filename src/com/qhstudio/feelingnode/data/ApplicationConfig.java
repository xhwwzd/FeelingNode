package  com.qhstudio.feelingnode.data;

import com.qhstudio.feelingnode.R;

/**
 *   配置文件
 *   
 */
public class ApplicationConfig {
	public final static long SplashTime=2000;    //闪屏等待时间
	public final static boolean SplashFallScreen=true;  //闪屏是否全屏
	public final static int SplashImage=R.drawable.ic_launcher;  //设置闪屏图片
	public final static String HOST="";
	
	public final static boolean UseGuide=false; //是否使用引导页面，true-使用引导  false-不使用引导  设置为true情况下GuideDrawable才有效果
	public final static int[] GuideDrawable={R.drawable.ic_launcher,
												R.drawable.ic_launcher,
													R.drawable.ic_launcher};  //引导页面显示配置
	
}
