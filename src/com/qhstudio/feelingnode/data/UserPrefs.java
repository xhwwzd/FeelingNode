package com.qhstudio.feelingnode.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 用户首选项 保存数据
 * 
 * @author xhwwzd
 *
 */
public class UserPrefs {

	Context context;
	SharedPreferences sp;

	public UserPrefs(Context context, String name) {
		this.context = context;
		sp = context.getSharedPreferences(name, Context.MODE_PRIVATE);
	}

	/**
	 * 保存整型数据
	 * 
	 * @param key
	 * @param value
	 */
	public void setInt(String key, int value) {
		sp.edit().putInt(key, value).commit();
	}

	/**
	 * 
	 * 获取整型数据
	 * 
	 * @param key
	 * @return
	 */
	public int getInt(String key, int def) {
		return sp.getInt(key, def);
	}

	/**
	 * 
	 * 保存字符数据
	 * 
	 * @param key
	 * @param value
	 */
	public void setString(String key, String value) {
		sp.edit().putString(key, value).commit();
	}

	/**
	 * 获取字符数据
	 * 
	 * @param key
	 * @return
	 */
	public String getStrin(String key, String def) {
		return sp.getString(key, def);
	}

	/**
	 * 保存布尔数据
	 * 
	 * @param key
	 * @param value
	 */
	public void setBoolean(String key, boolean value) {
		sp.edit().putBoolean(key, value).commit();
	}

	/**
	 * 获取布尔数据
	 * 
	 * @param key
	 * @return
	 */
	public boolean getBoolean(String key, boolean def) {
		return sp.getBoolean(key, def);
	}

	/**
	 * 
	 * 保存Long型
	 * 
	 * @param key
	 * @param value
	 */
	public void setLong(String key, long value) {
		sp.edit().putLong(key, value).commit();
	}

	/**
	 *
	 * 获取Long型
	 * 
	 * @param key
	 * @return
	 */
	public long getLong(String key, long def) {
		return sp.getLong(key, def);
	}
}
