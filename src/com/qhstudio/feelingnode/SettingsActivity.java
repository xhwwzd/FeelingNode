package com.qhstudio.feelingnode;

import com.qhstudio.feelingnode.base.BaseActivity;
import com.qhstudio.feelingnode.bean.User;
import com.qhstudio.feelingnode.bean.WebParam;
import com.qhstudio.feelingnode.tools.ActivityResources;
import com.qhstudio.feelingnode.views.ItemGroup;
import com.qhstudio.feelingnode.views.ItemGroup.onItemClick;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class SettingsActivity extends BaseActivity {

	Button login;

	String loginLable, logoutLable;
	WebParam helpParam, aboutParam;

	@Override
	public void start() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.activity_settings;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		setNoti();
		login = (Button) getObject(R.id.setting_login);
		setTitle(R.id.activity_title);

		helpParam = new WebParam();
		helpParam.setTitle("帮助中心");
		helpParam.setUrl("file:///android_asset/PoolNote/help.html");

		aboutParam = new WebParam();
		aboutParam.setTitle("关于我们");
		aboutParam.setUrl("file:///android_asset/PoolNote/about.html");

		ItemGroup group = (ItemGroup) findViewById(R.id.settings_group);

		group.addLine(0, 0, 40, 0, 1);
		group.addItem(R.drawable.ic_help, getString(R.string.setting_lable_help), "", new onItemClick() {

			@Override
			public void itemClick(int click, String lable) {
				// TODO Auto-generated method stub

				ActivityResources.toActivity(SettingsActivity.this, WebPlayer.class, WebPlayer.WebKey, helpParam);
			}
		});
		group.addLine(0, 0, 0, 0, 1);
		group.addItem(R.drawable.s2, getString(R.string.setting_lable_advice), "", new onItemClick() {

			@Override
			public void itemClick(int click, String lable) {
				// TODO Auto-generated method stub
				ActivityResources.toActivity(SettingsActivity.this, AdviceActivity.class);
			}
		});
		group.addLine(0, 0, 0, 0, 1);
		group.addItem(R.drawable.ic_share, getString(R.string.setting_lable_share), "", new onItemClick() {

			@Override
			public void itemClick(int click, String lable) {
				// TODO Auto-generated method stub
			}
		});
		group.addLine(0, 0, 0, 0, 1);

		group.addLine(0, 0, 20, 0, 1);
		group.addItem(R.drawable.ic_check_new, getString(R.string.setting_lable_new), "V1.0.0", new onItemClick() {

			@Override
			public void itemClick(int click, String lable) {
				// TODO Auto-generated method stub
			}
		});
		group.addLine(0, 0, 0, 0, 1);
		group.addItem(R.drawable.s3, getString(R.string.setting_lable_about), "", new onItemClick() {

			@Override
			public void itemClick(int click, String lable) {
				// TODO Auto-generated method stub
				ActivityResources.toActivity(SettingsActivity.this, WebPlayer.class, WebPlayer.WebKey, aboutParam);
			}
		});
		group.addLine(0, 0, 0, 0, 1);

		group.addLine(0, 0, 20, 0, 1);
		group.addItem(R.drawable.s5, getString(R.string.setting_lable_hot), "", new onItemClick() {

			@Override
			public void itemClick(int click, String lable) {
				// TODO Auto-generated method stub
			}
		});
		group.addLine(0, 0, 0, 0, 1);

		group.invalidate();
		setInfo();
	}

	void setInfo() {
		loginLable = getString(R.string.login_lable);
		logoutLable = getString(R.string.logout_lable);

		if (User.getInstance() == null)
			return;
		if (User.getInstance().getBean().getIsLogin()) {
			login.setText(logoutLable);
		} else {
			login.setText(loginLable);
		}
	}

	public void userLogin(View button) {
		Intent login = new Intent(SettingsActivity.this, UserLoginActivity.class);
		startActivity(login);
	}

}
