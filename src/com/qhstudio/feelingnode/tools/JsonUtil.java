package com.qhstudio.feelingnode.tools;

import org.json.JSONObject;

import com.google.gson.Gson;

/**
 * Json解析
 * @author xhwwzd
 *
 */
public class JsonUtil {

	public static Object ToBean(Class<?> bean, String json) {
		Gson gson = new Gson();
		Object jsonBean = gson.fromJson(json, bean);
		return jsonBean;
	}

	public static Object creatBean(Class<?> bean, JSONObject obj) {
		Gson gson = new Gson();
		Object item = gson.fromJson(obj.toString(), bean);
		return item;
	}

	public static String toJson(Object obj) {
		Gson gson = new Gson();
		String reString = gson.toJson(obj);
		return reString;
	}
}
