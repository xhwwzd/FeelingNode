package com.qhstudio.feelingnode.tools;

import java.io.Serializable;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ActivityResources {

	public static void toActivity(Context from, Class<?> to) {
		toActivity(from, to, null, null);
	}

	public static void toActivity(Context from, Class<?> to, String key, Serializable obj) {
		Intent detal = new Intent(from, to);
		if (obj != null) {
			Bundle temp = new Bundle();
			if (key == null || key.length() < 1) {
				key = obj.getClass().getName();
			}
			temp.putSerializable(key, obj);
			detal.putExtras(temp);
		}
		from.startActivity(detal);
	}

	public static void toActivity(Context from, Class<?> to, Map<String, String> map) {
		Intent intent = new Intent(from, to);
		Bundle bundle = new Bundle();
		for (Map.Entry<String, String> enter : map.entrySet()) {
			intent.putExtra(enter.getKey(), enter.getValue());
			bundle.putString(enter.getKey(), enter.getValue());
		}
		intent.putExtras(bundle);
		from.startActivity(intent);
	}
}
