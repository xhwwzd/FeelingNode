package com.qhstudio.feelingnode;

import com.qhstudio.feelingnode.base.BaseActivity;

import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class AdviceActivity extends BaseActivity {

	Spinner adviceSpinner;

	@Override
	public void start() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.activity_advice;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		setNoti();
		setTitle(R.id.activity_title);
		adviceSpinner = (Spinner) getObject(R.id.advice_spnner);

		String meinv[] = getResources().getStringArray(R.array.advice_type);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, meinv);
		adapter.setDropDownViewResource(R.layout.view_spinner_item);
		adviceSpinner.setAdapter(adapter);
		
		
	}

}
