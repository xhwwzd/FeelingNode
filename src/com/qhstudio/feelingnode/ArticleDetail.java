package com.qhstudio.feelingnode;

import com.qhstudio.feelingnode.base.BaseActivity;
import com.qhstudio.feelingnode.bean.ArticleBean;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ArticleDetail extends BaseActivity {

	TextView titleText;
	ArticleBean articleBean;

	@Override
	public void start() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.activity_article_detail;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		setNoti();
		Bundle temp = getIntent().getExtras();
		if (temp != null) {
			articleBean = (ArticleBean) temp.get("info");
		}
		titleText = getTextView(R.id.activity_title);
		setView(articleBean);
		Log.e("-----read title", getTitle().toString());
	}

	void setView(ArticleBean bean) {
		if (bean == null)
			return;
		titleText.setText(bean.getTitle());
		
	}

}
