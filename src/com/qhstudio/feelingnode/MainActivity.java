package com.qhstudio.feelingnode;

import com.qhstudio.feelingnode.base.BaseFragmentActivity;
import com.qhstudio.feelingnode.fragment.CicleFragment;
import com.qhstudio.feelingnode.fragment.HomeFragment;
import com.qhstudio.feelingnode.fragment.SelfFragment;
import com.qhstudio.feelingnode.views.TabGroup;
import com.qhstudio.feelingnode.views.TabGroup.CheckListener;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends BaseFragmentActivity {

	TabGroup mainTab;
	HomeFragment homeFragment;
	CicleFragment cicleFragment;
	SelfFragment selfFragment;
	private long exitTime = 0;// 双击退出计时

	String quit;

	@Override
	public void start() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.activity_main;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		setNoti();
		init();
	}

	void init() {

		mainTab = (TabGroup) findViewById(R.id.main_tab);
		mainTab.TextColor = R.color.menu_text_color;
		mainTab.TextSize=12;
		mainTab.addTab(R.drawable.home_nomal, R.drawable.home_active,
				getString(R.string.main_tab_home));
		mainTab.addTab(R.drawable.ic_menu_2_nomal, R.drawable.ic_menu_2_click,
				getString(R.string.main_tab_circle));
		mainTab.addTab(R.drawable.min_nomal, R.drawable.min_active,
				getString(R.string.main_tab_self));
		

		mainTab.addListener(new CheckListener() {

			@Override
			public void checkListener(int postion) {
				// TODO Auto-generated method stub
				switch (postion) {
				case 0:
					showFragment(homeFragment);
					break;
				case 1:
					showFragment(cicleFragment);
					break;
				case 2:
					showFragment(selfFragment);
					break;
				}
			}
		});

		mainTab.commit();

		homeFragment = new HomeFragment(getSupportFragmentManager());
		cicleFragment = new CicleFragment();
		selfFragment = new SelfFragment();

		addFragment(R.id.main_fragment, homeFragment);
		addFragment(R.id.main_fragment, cicleFragment);
		addFragment(R.id.main_fragment, selfFragment);

		mainTab.setChecked(0, true);

		quit = getString(R.string.action_lable_quit);
	}

	/**
	 * 进入设置
	 * 
	 * @param v
	 */
	public void toSettings(View v) {
		Intent settings = new Intent(this, SettingsActivity.class);
		startActivity(settings);
	}
	/**
	 * 发送
	 */
	public void SendArticle(View v) {
		Intent send = new Intent(this, SendArticleActivity.class);
		startActivity(send);
	}
	/**
	 * 退出内容
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if ((System.currentTimeMillis() - exitTime) > 1500) {
			Toast.makeText(this, quit, Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			super.onBackPressed();

		}
	}
}
