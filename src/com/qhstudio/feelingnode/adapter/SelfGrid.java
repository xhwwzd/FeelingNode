package com.qhstudio.feelingnode.adapter;

import java.util.ArrayList;
import java.util.List;

import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.base.SuperAdapter;
import com.qhstudio.feelingnode.bean.SelfGridBean;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

public class SelfGrid extends SuperAdapter<SelfGridBean> {

	List<SelfGridBean> gridBean = new ArrayList<SelfGridBean>();
	int bg = R.drawable.rect_trb_line_click;
	int bg2 = R.drawable.rect_rb_line_click;

	public SelfGrid(Context context) {
		super(context);
		// TODO Auto-generated constructor stub

		SelfGridBean lock = new SelfGridBean(R.drawable.ic_self_g0, "本地");
		SelfGridBean safe = new SelfGridBean(R.drawable.ic_security, "安全");
		SelfGridBean scb = new SelfGridBean(R.drawable.ic_book, "账号");
		SelfGridBean g3 = new SelfGridBean(R.drawable.ic_self_cicle, "社交");
		SelfGridBean g4 = new SelfGridBean(R.drawable.ic_self_g4, "圈子");
		SelfGridBean g5 = new SelfGridBean(R.drawable.ic_self_g5, "商店");
		SelfGridBean g6 = new SelfGridBean(R.drawable.ic_self_g6, "消息");
		SelfGridBean g7 = new SelfGridBean(R.drawable.ic_self_g7, "附近");
		SelfGridBean g8 = new SelfGridBean(R.drawable.ic_self_g8, "好友");

		gridBean.add(lock);
		gridBean.add(safe);
		gridBean.add(scb);
		gridBean.add(g3);
		gridBean.add(g4);
		gridBean.add(g5);
		gridBean.add(g6);
		gridBean.add(g7);
		gridBean.add(g8);

		setList(gridBean);
	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.view_self_item;
	}

	@Override
	public void setChildView(int position, SelfGridBean data, SuperAdapter.ViewHolder holder) {
		// TODO Auto-generated method stub
		if (position < 3)
			holder.view.setBackgroundResource(bg);
		else
			holder.view.setBackgroundResource(bg2);

		TextView lable = (TextView) holder.getObject(R.id.self_grid_lable);
		lable.setText(data.lable);

		ImageView icon = (ImageView) holder.getObject(R.id.self_grid_icon);
		if (data.icon != -1)
			icon.setImageResource(data.icon);

	}

}
