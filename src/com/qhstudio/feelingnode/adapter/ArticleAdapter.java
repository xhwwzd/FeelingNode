package com.qhstudio.feelingnode.adapter;

import java.util.ArrayList;
import java.util.List;

import com.qhstudio.feelingnode.R;
import com.qhstudio.feelingnode.base.SuperAdapter;
import com.qhstudio.feelingnode.bean.ArticleBean;

import android.content.Context;
import android.util.Log;

public class ArticleAdapter extends SuperAdapter<ArticleBean> {

	public ArticleAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		List<ArticleBean> list = new ArrayList<ArticleBean>();
		for (int a = 0; a < 10; a++) {
			ArticleBean bean = new ArticleBean();
			bean.setArticle("测试内容" + a);
			bean.setId(a);
			bean.setLike(4 * a);
			bean.setSender("测试人员"+a+">_<");
			bean.setTalkCount(2 * a);
			bean.setShareCount(a);
			Log.e("info", ">>>>"+a);
			list.add(bean);
		}

		setList(list);
	}

	@Override
	public int getLayout() {
		// TODO Auto-generated method stub
		return R.layout.view_article_item;
	}

	@Override
	public void setChildView(int position, ArticleBean data,
			SuperAdapter.ViewHolder holder) {
		// TODO Auto-generated method stub
		holder.getTextView(R.id.article_sender).setText(data.getSender());
		holder.getTextView(R.id.article_time).setText(data.getTime());
		holder.getTextView(R.id.article_info).setText(data.getArticle());
		holder.getTextView(R.id.article_share).setText(data.getShareCount()+"");
		holder.getTextView(R.id.article_talk).setText(data.getTalkCount()+"");
		holder.getTextView(R.id.article_like).setText(data.getLike()+"");
	}

}
